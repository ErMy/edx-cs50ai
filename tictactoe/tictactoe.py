"""
Tic Tac Toe Player
"""

import math
import copy

X = "X"
O = "O"
EMPTY = None


def initial_state():
    """
    Returns starting state of the board.
    """
    return [[EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY]]


def player(board):
    """
    Returns player who has the next turn on a board.
    """
    count = 0
    for row in board:
        for cell in row:
            if cell == EMPTY:
                count += 1

    if count % 2 != 0:
        return X
    else:
        return O


def actions(board):
    """
    Returns set of all possible actions (i, j) available on the board.
    """
    moves = set()
    for row in range(len(board)):
        for cell in range(len(board[row])):
            if board[row][cell] == EMPTY:
                moves.add((row, cell))
    return moves


def result(board, action):
    """
    Returns the board that results from making move (i, j) on the board.
    """
    new_board = copy.deepcopy(board)
    if new_board[action[0]][action[1]] == EMPTY:
        new_board[action[0]][action[1]] = player(board)
        return new_board
    else:
        raise AssertionError("Move not allowed")


def winner(board):
    """
    Returns the winner of the game, if there is one.
    """
    for row in board:
        if row[0] == row[1] and row[1] == row[2]:
            return row[0]
    for column in range(len(board)):
        if board[0][column] == board[1][column] and board[1][column] == board[2][column]:
            return board[0][column]
    if board[0][0] == board[1][1] and board[1][1] == board[2][2]:
        return board[0][0]
    if board[0][2] == board[1][1] and board[1][1] == board[2][0]:
        return board[0][2]
    return None


def terminal(board):
    """
    Returns True if game is over, False otherwise.
    """
    if winner(board) is not None:
        return True
    for row in board:
        for cell in row:
            if cell == EMPTY:
                return False
    return True


def utility(board):
    """
    Returns 1 if X has won the game, -1 if O has won, 0 otherwise.
    """
    victor = winner(board)
    if victor == X:
        return 1
    elif victor == O:
        return -1
    else:
        return 0


def minimax(board):
    """
    Returns the optimal action for the current player on the board.
    """
    if player(board) == X:
        move, score, alpha, beta = max_alpha_beta(board, -math.inf, math.inf)
    else:
        move, score, alpha, beta = min_alpha_beta(board, -math.inf, math.inf)
    return move


def max_alpha_beta(board, alpha, beta):
    moves = actions(board)
    highest_score = -math.inf
    best_move = ()
    for move in moves:
        result_board = result(board, move)
        if terminal(result_board):
            score = utility(result_board)
        else:
            _move, score, _alpha, _beta = min_alpha_beta(result_board, alpha, beta)
            if _alpha > alpha:
                alpha = _alpha
        if score > beta:
            highest_score = score
            break
        if score > highest_score:
            highest_score = score
            best_move = move
    return best_move, highest_score, alpha, beta


def min_alpha_beta(board, alpha, beta):
    moves = actions(board)
    highest_score = math.inf
    best_move = ()
    for move in moves:
        result_board = result(board, move)
        if terminal(result_board):
            score = utility(result_board)
        else:
            _move, score, _alpha, _beta = max_alpha_beta(result_board, alpha, beta)
            if _beta < beta:
                beta = _beta
        if score < alpha:
            highest_score = score
            break
        if score < highest_score:
            highest_score = score
            best_move = move
    return best_move, highest_score, highest_score, beta
