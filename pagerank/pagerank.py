import os
import random
import re
import sys
import copy

DAMPING = 0.85
SAMPLES = 10000


def main():
    if len(sys.argv) != 2:
        sys.exit("Usage: python pagerank.py corpus")
    corpus = crawl(sys.argv[1])
    ranks = sample_pagerank(corpus, DAMPING, SAMPLES)
    print(f"PageRank Results from Sampling (n = {SAMPLES})")
    for page in sorted(ranks):
        print(f"  {page}: {ranks[page]:.4f}")
    ranks = iterate_pagerank(corpus, DAMPING)
    print(f"PageRank Results from Iteration")
    for page in sorted(ranks):
        print(f"  {page}: {ranks[page]:.4f}")


def crawl(directory):
    """
    Parse a directory of HTML pages and check for links to other pages.
    Return a dictionary where each key is a page, and values are
    a list of all other pages in the corpus that are linked to by the page.
    """
    pages = dict()

    # Extract all links from HTML files
    for filename in os.listdir(directory):
        if not filename.endswith(".html"):
            continue
        with open(os.path.join(directory, filename)) as f:
            contents = f.read()
            links = re.findall(r"<a\s+(?:[^>]*?)href=\"([^\"]*)\"", contents)
            pages[filename] = set(links) - {filename}

    # Only include links to other pages in the corpus
    for filename in pages:
        pages[filename] = set(
            link for link in pages[filename]
            if link in pages
        )

    return pages


def transition_model(corpus, page, damping_factor):
    """
    Return a probability distribution over which page to visit next,
    given a current page.

    With probability `damping_factor`, choose a link at random
    linked to by `page`. With probability `1 - damping_factor`, choose
    a link at random chosen from all pages in the corpus.
    """
    prob_distribution = {}
    for key in corpus:
        prob_distribution[key] = (1-damping_factor) / len(corpus.keys())
        if len(corpus[page]) == 0:
            prob_distribution[key] += damping_factor / len(corpus.keys())
        elif key in corpus[page]:
            prob_distribution[key] += damping_factor / len(corpus[page])
    return prob_distribution

def sample_pagerank(corpus, damping_factor, n):
    """
    Return PageRank values for each page by sampling `n` pages
    according to transition model, starting with a page at random.

    Return a dictionary where keys are page names, and values are
    their estimated PageRank value (a value between 0 and 1). All
    PageRank values should sum to 1.
    """
    page_ranks = {}
    for key in corpus:
        page_ranks[key] = 0
    page = random.choice(list(corpus.keys()))
    page_ranks[page] += 1
    for i in range(1, n):
        model = transition_model(corpus, page, damping_factor)
        pick = random.random()
        for key in page_ranks:
            pick -= model[key]
            if pick < 0:
                page = key
                page_ranks[page] += 1
                break
    for key in page_ranks:
        page_ranks[key] /= n
    return page_ranks


def iterate_pagerank(corpus, damping_factor):
    """
    Return PageRank values for each page by iteratively updating
    PageRank values until convergence.

    Return a dictionary where keys are page names, and values are
    their estimated PageRank value (a value between 0 and 1). All
    PageRank values should sum to 1.
    """
    page_ranks = {}
    for key in corpus:
        page_ranks[key] = 1 / len(corpus.keys())
    prev_ranks = copy.deepcopy(page_ranks)
    threshold = 0.001
    while True:
        iterate = False
        for key in page_ranks:
            weighted_sum = 0
            for page in prev_ranks:
                if key in corpus[page]:
                    weighted_sum += prev_ranks[page] / len(corpus[page])
                elif len(corpus[page]) == 0:
                    weighted_sum += prev_ranks[page] / len(corpus.keys())
            page_ranks[key] = (1-damping_factor)/len(corpus.keys()) + damping_factor * weighted_sum
            if abs(page_ranks[key] - prev_ranks[key]) > threshold:
                iterate = True
        if not iterate:
            break
        prev_ranks = copy.deepcopy(page_ranks)
    return page_ranks


if __name__ == "__main__":
    main()
